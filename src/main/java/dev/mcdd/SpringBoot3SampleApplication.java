package dev.mcdd;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author mcdd1024
 * @since 0.0.1
 */
@Slf4j
@SpringBootApplication
public class SpringBoot3SampleApplication implements ApplicationRunner {

    public static void main(String[] args) {
        var app = new SpringApplication(SpringBoot3SampleApplication.class);
        // config app for example [app type,shutdown hook etc.]
        app.setWebApplicationType(WebApplicationType.SERVLET);
        app.run(args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        try {
            logger.info("SpringBoot3SampleApplication.run");
        } catch (Exception ex) {
            throw new Exception("SpringBoot3SampleApplication.run failed", ex);
        }
    }
}
