# spring-boot3-sample

repository `https://gitcode.com/mcdd/spring-boot3-sample`

# version

- zulu jdk 21
- idea 2024.x
- maven 3.8.x
- spring boot 3.4.1

# doc

- [spring-boot3-sample](./docs/README.md)

# ref

- [ALiYun Maven](https://developer.aliyun.com/mirror/maven)
- [Lombok config](https://projectlombok.org/features/configuration)
- [Maven Repository](https://mvnrepository.com/)
- [Spring Boot](https://docs.spring.io/spring-boot/index.html)
- [Spring Boot cn](https://springdoc.cn/spring-boot/)